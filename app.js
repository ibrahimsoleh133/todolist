require('dotenv').config()
const express = require('express')
const app = express()
const port =  5000;
const router = require('./routes');

app.use(express.json())
app.use(express.urlencoded({extended: true}));

app.use(router)

if (process.env.NODE_ENV !== 'test') {
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
      })
}
module.exports = app;