const app = require("../app");
const request = require("supertest");

describe("GET all /todo", function () {
  it("responds with json", function (done) {
    request(app)
      .get("/todo")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const data = response.body[0];
        expect(data.id).toBe(3);
        expect(data.title).toBe("tidur");
        expect(response.status).toEqual(200);
        // console.log(data,'|||||||||||||||||||||||||||');
        // expect(response.body.data.length >= 1).toBe(true);
        done();
      });
  });

  it("test get by id, todo/:id", function (done) {
    request(app)
      .get("/todo/1")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.id).toBeDefined();
        expect(response.body.title).toBe('makan')
        expect(response.body.status).toBe('Not Active')
        // console.log(response.body,'||||||||||||||||||||||||||||||');
        done();
      })
      .catch(done);
  });

  it("post todo, /todo", function (done) {
    request(app)
      .post("/todo")
      .send({ title: "john", status: "active" })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        const data = response.body;
        // console.log(data.title,'>>>>>>>>>>>>>');
        console.log(response.status);
        expect(response.status).toEqual(201);
        expect(data.status).toBe("active");
        expect(data.title).toBe("john");
        done();
      })
      .catch(done);
  });

  it("soft delete /todo/:id", function (done) {
    request(app)
      .delete("/todo/1")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        console.log(response.body.message);
        expect(response.body.status).not.toBe(0);
        expect(response.status).toEqual(200);
        expect(response.body.message).toContain("Delete successfully");
        done()
      })
      .catch(done);
  });
});
