const express = require('express');
const router = express.Router();
const TodoControllers = require('../controllers/todoControllers')

router.get("/todo", TodoControllers.findAllTodo);
router.get("/todo/:id", TodoControllers.getTodoById);
router.post("/todo", TodoControllers.createTodo);
router.delete("/todo/:id", TodoControllers.deleteTodo);

module.exports = router;