const {todo} = require('../models');

class TodoControllers {
    static findAllTodo = async(req, res) => {
        const data = await todo.findAll({
            where: {status:"active"}
        });
        res.status(200).json(data);
    }
    static getTodoById = async (req, res) => {
        const {id} = req.params;
        const data = await todo.findOne({
            where: {id}
        })
        res.status(200).json(data)
    }
    static createTodo = async (req, res) => {
        const {title, status} = req.body;
            const data = await todo.create({
                    title,
                    status
                })
                res.status(201).json(data);
    }
    static deleteTodo = async (req, res) => {
        const {id} = req.params;
        const data = await todo.update({
            status: "Not Active"
                }, {
                    where: {id}
                })
                res.status(200).json({
                    message: "Delete successfully"
                })
    }
}

module.exports = TodoControllers;